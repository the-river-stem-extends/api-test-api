# 基于python-flask生态 + HttpRunner 开发的rest风格的测试平台后端

## 线上预览：http://106.55.164.201/#/login  账号：tester、密码：123456

## 前端传送门：https://gitee.com/Xiang-Qian-Zou/api-test-front

## Python版本：python => 3.6+

### 1.安装依赖包：sudo pip install -i https://pypi.douban.com/simple/ -r requirements.txt

### 2.创建MySQL数据库，数据库名自己取，编码选择utf8mb4，对应config.yaml下db配置为当前数据库信息即可

### 3.初始化数据库表结构（项目根目录下依次执行下面3条命令）：
    sudo python dbMigration.py db init
    sudo python dbMigration.py db migrate
    sudo python dbMigration.py db upgrade

### 4.初始化权限、角色、管理员（项目根目录下执行，账号：admin，密码：123456）
    sudo python dbMigration.py init

### 5.生产环境下的一些配置: 
    1.把后端端口改为8024启动
    2.为避免定时任务重复触发，需关闭debug模式（debug=False或去掉debug参数）
    3.准备好前端包，并在nginx.location / 下指定前端包的路径
    4.直接把项目下的nginx.conf文件替换nginx下的nginx.conf文件
    5.nginx -s reload 重启nginx

### 6.启动项目
    开发环境: python manage.py
    生产环境: 
        使用配置文件: sudo nohup gunicorn -c gunicornConfig.py main:app &
        不使用配置文件: sudo nohup gunicorn -w 3 -b 0.0.0.0:8024 main:app –preload &
    
    如果报 gunicorn 命令不存在，则先找到 gunicorn 安装目录，创建软连接
    ln -s /usr/local/python3/bin/gunicorn /usr/bin/gunicorn
    ln -s /usr/local/python3/bin/gunicorn  /usr/local/bin/gunicorn
    sudo nohup /usr/local/bin/gunicorn -w 1 -b 0.0.0.0:8024 main:app –preload &

### 修改依赖后创建依赖：sudo pip freeze > requirements.txt


### QQ交流群：249728408
