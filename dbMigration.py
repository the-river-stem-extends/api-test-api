#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2020/9/25 17:13
# @Author : ZhongYeHai
# @Site :
# @File : dbMigration.py
# @Software: PyCharm

from collections import OrderedDict

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app.baseModel import db
from app.api.apiMsg.models import ApiMsg
from app.api.case.models import Case
from app.api.step.models import Step
from app.api.sets.models import Set
from app.api.func.models import Func
from app.api.module.models import Module
from app.api.project.models import Project
from app.api.report.models import Report
from app.api.task.models import Task, ApschedulerJobs
from app.api.user.models import User, Permission, Role
from main import app

manager = Manager(app)

Migrate(app, db)
manager.add_command('db', MigrateCommand)


@manager.command
def init_admin():
    """ 初始化管理员用户 """
    if User.query.filter_by(name='管理员').first():
        print(f'{"=" * 15} 已存在管理员用户，可直接登录 {"=" * 15}')
        return
    else:
        print(f'{"=" * 15} 开始创建管理员用户 {"=" * 15}')
        with db.auto_commit():
            user = User()
            user.account = 'admin'
            user.password = '123456'
            user.name = '管理员'
            user.status = 1
            user.role_id = 2
            user.create_user = 1
            db.session.add(user)
        print(f'{"=" * 15} 创建管理员用户成功 {"=" * 15}')


@manager.command
def init_role():
    """ 初始化角色 """
    print(f'{"=" * 15} 开始创建角色 {"=" * 15}')
    roles_permissions_map = OrderedDict()
    roles_permissions_map[u'测试人员'] = ['COMMON']
    roles_permissions_map[u'管理员'] = ['COMMON', 'ADMINISTER']
    for role_name in roles_permissions_map:
        role = Role.get_first(name=role_name)
        if role is None:
            role = Role(name=role_name)
            db.session.add(role)
            role.permission = []
        for permission_name in roles_permissions_map[role_name]:
            permission = Permission.get_first(name=permission_name)
            if permission is None:
                permission = Permission(name=permission_name)
                db.session.add(permission)
            role.permission.append(permission)
            db.session.commit()
    print(f'{"=" * 15} 角色创建成功 {"=" * 15}')


@manager.command
def init():
    """ 初始化 权限、角色、管理员 """
    print(f'{"=" * 15} 正在初始化 权限、角色、管理员 {"=" * 15}')
    init_role()
    init_admin()
    print(f'{"=" * 15} 已创建 权限、角色、管理员 {"=" * 15}')


"""
初始化数据库
python dbMigration.py db init
python dbMigration.py db migrate
python dbMigration.py db upgrade

初始化数据
python dbMigration.py init
"""

if __name__ == '__main__':
    manager.run()
